# frozen_string_literal: true

require "active_support/concern"

module Decidim
  module ExtraUserFields
    module SurveysControllerOverrides
      extend ActiveSupport::Concern

      # If user is not logged in, permissions does not matter
      def check_permissions
        return unless current_user

        super
      end
    end
  end
end
